from os import getcwd, makedirs, listdir, getenv
from os.path import abspath, join, splitext, isfile, isdir, exists
import subprocess

OUTPATH = getenv("OUTPUT_DIR", "extracted/")


""" Subproces extract file """
def extract(compressed_file, cwd=getcwd(), compressed_dir=".", pipe=True, output_dir=OUTPATH, decompressed_dir=""):
    directory = abspath(join(output_dir, decompressed_dir))
    tar = abspath(join(compressed_dir, compressed_file))

    if not isfile(tar):
        raise Exception(f"Compress Dir is not a file or does not exist: {tar}")

    # if not isdir(output_dir):
    #     # Throw Exception
    #     raise Exception(f"Output Dir is not a directory: {output_dir}")

    if not isdir(cwd):
        raise Exception(f"Specify CWD is not valid: {cwd}")

    if not exists(directory):
        makedirs(directory, mode=0o666)

    opt = dict(stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    opt = opt if pipe else {}
    out = subprocess.run(['tar', '-xzvf', tar, "-C", directory], cwd=cwd, **opt)
    code = int(out.returncode)

    if code != 0:
        output = out.stdout.decode('utf-8') if pipe else 'See above'
        raise Exception(f'exit {code}\nExtraction Failed: ' + output)
    yield [ abspath(join(directory, f)) for f in listdir(directory) if isfile(join(directory, f)) ]

for x in extract("pdf.tar.gz", compressed_dir="./sample/pdf/"):
    print(x)