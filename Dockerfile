# FROM panteparak/wait-for as temp
# FROM python:3.7-alpine

# RUN apk add --update --no-cache bash tar

# COPY --from=temp /wait-for-it /bin/wait-for-it
# RUN chmod +x /bin/wait-for-it
# RUN ls -la /bin | grep wait-for-it
# COPY . .
# CMD wait-for-it www.google.com:443 -s -- echo "Google is up!" && python app.py

#FROM openjdk:8-alpine
#RUN apk add  --update --no-cache poppler-utils bash
#RUN hg clone http://hg.openjdk.java.net/shenandoah/jdk8u shenandoah && cd shenandoah/ && ls -la && chmod +x get_source.sh configure && ./get_source.sh && ./configure && make images && build/linux-x86_64-normal-server-release/images/j2sdk-image/bin/java -XX:+UseShenandoahGC -version

FROM shipilev/openjdk-shenandoah:8
RUN apt update -y && apt install -y poppler-utils

WORKDIR /app
ADD target/scala-2.12/pdfparser-assembly-0.1.jar pdfparser.jar
#ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh wait-for-it
#RUN chmod +x wait-for-it
#CMD ./wait-for-it minio:9000 -s -t 30 -- ./wait-for-it rabbitmq:5672 -s -t 30 -- java -Xms512m -Xmx2g -jar pdfparser.jar
CMD java -XX:+UseShenandoahGC -Xms512m -Xmx2g -XX:+UnlockExperimentalVMOptions -XX:ShenandoahUncommitDelay=10000 -XX:ShenandoahGuaranteedGCInterval=10000 -jar pdfparser.jar
