name := "pdfparser"

version := "0.1"

scalaVersion := "2.12.7"

resolvers += "Artifactory" at "https://artifact.teparak.me/artifactory/backendtech/"
libraryDependencies += "commons-io" % "commons-io" % "2.6"

libraryDependencies += "com.google.code.gson" % "gson" % "2.8.5"

libraryDependencies += "org.mongodb" % "bson" % "3.8.2"

libraryDependencies += "me.teparak" % "minio" % "1.2.1"
libraryDependencies += "me.teparak" % "rabbitmq" % "1.1.3"

libraryDependencies ++= Seq("org.slf4j" % "slf4j-api" % "1.7.5", "org.slf4j" % "slf4j-simple" % "1.7.5")

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
