package teparak.me.backendtech.p2.pdfparser

import com.google.gson.{JsonObject, JsonParser}
import me.teparak.rabbit.utils.Rabbit


object test {
  val rabbit = new Rabbit("localhost", 5672)
  var json: JsonObject = new JsonObject()
  json.addProperty("job_id","12345")
  json.addProperty("file_id","abcdde")

  println("Pub")
  rabbit.publish(json, "parse")
  println("Done")
}

