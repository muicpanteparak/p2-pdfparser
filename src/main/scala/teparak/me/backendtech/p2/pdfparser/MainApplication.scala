package teparak.me.backendtech.p2.pdfparser
import sys.process._
import java.io.File
import java.nio.file.Paths

import com.google.gson.JsonObject
import me.teparak.minio.utils.MinioWrapper
import me.teparak.rabbit.utils.Rabbit
import org.apache.commons.io.{FileUtils, FilenameUtils}
import org.slf4j.LoggerFactory

import scala.util.Properties.envOrElse

object MainApplication extends App {
  private val Logger = LoggerFactory.getLogger("MainApplication")
  private val MINIO_USER = envOrElse("MY_MINIO_USER", "admin")
  private val MINIO_PASS = envOrElse("MY_MINIO_PASS", "password")
  private val MINIO_HOST = envOrElse("MY_MINIO_HOST", "localhost")
  private val MINIO_PORT = envOrElse("MY_MINIO_PORT", "9000").toInt

  private val RABBIT_USER = envOrElse("MY_RABBIT_USER", "guest")
  private val RABBIT_PASS = envOrElse("MY_RABBIT_PASS", "guest")
  private val RABBIT_HOST = envOrElse("MY_RABBIT_HOST", "localhost")
  private val RABBIT_PORT = envOrElse("MY_RABBIT_PORT", "5672").toInt

  private val saveLocation = "/tmp"
  println(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  println(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)
  private val rabbit = new Rabbit(RABBIT_HOST, RABBIT_PORT, RABBIT_USER, RABBIT_PASS)
  private val minio = new MinioWrapper(MINIO_HOST, MINIO_PORT, MINIO_USER, MINIO_PASS)
  private val MINIO_PDF_LOC = "pdf/"
  private val MINIO_TXT_LOC = "txt/"
  private val PDF_EXTENSION = ".pdf"
  private val TXT_EXTENSION = ".txt"

  def subprocess(pdfFile: File, textFile: File) = {
    Logger.debug(f"Executing subprocess PDF -> $pdfFile, txt -> $textFile")
    if (!pdfFile.canRead) throw new FilePermissionException("Unable to read file")
    if (!pdfFile.isFile) throw new IllegalArgumentException("PDF Path is not to a file")

    if (textFile.exists) {
      textFile.delete()
    }

    val exitCode = Seq("pdftotext", "-layout", pdfFile.toString, textFile.toString).!
    exitCode match {
      case 0 => exitCode
      case s => throw new ExecutionFailedException(s"program exit with $s code")
    }
  }

  def downloadFile(file: File, jobId: String, objectId: String) = {
    println(jobId, objectId)
    val in = minio.download(jobId, objectId)
    FileUtils.copyInputStreamToFile(in, file)
  }

  def uploadFile(file: File, jobId: String, objectId: String) = {
    minio.upload(jobId, objectId, file)
  }

  def createFile(jobId: String, fileId: String, ext: ObjectType): File = {
    Paths.get(saveLocation, jobId, fileId ).toFile
  }

  def cleanup(jobId: String, fileId: String) = {
    new File(saveLocation, jobId).delete()
  }

  def consumer(json: JsonObject) = {
    Logger.info("consuming...")

    // TODO: Handle null case
    val jobId = json.get("jobId").getAsString
    val fileName = json.get("fileName").getAsString // No extension

    val PDF_NAME = getPDFName(fileName)
    val TXT_NAME = getTXTName(fileName)
    Logger.info(f"Consuming job: $jobId/$fileName")

    val pdf = createFile(jobId, PDF_NAME, PDF)
    val txt = createFile(jobId, TXT_NAME, TEXT)

    statusUpdate(jobId, fileName, false)
    downloadFile(pdf, jobId, MINIO_PDF_LOC + PDF_NAME)

    try{
      subprocess(pdf, txt)
      uploadFile(txt, jobId, MINIO_TXT_LOC + TXT_NAME)

      cleanup(jobId, PDF_NAME)
      cleanup(jobId, TXT_NAME)
      statusUpdate(jobId, fileName, true)
      statusUpdate(jobId, fileName, true)
    } catch {
      case e: Exception =>
        val f = File.createTempFile("empty", ".txt")
        uploadFile(f, jobId, MINIO_TXT_LOC + TXT_NAME)
        statusUpdate(jobId, fileName)
    }
  }

  def getPDFName(string: String): String = {
    FilenameUtils.getBaseName(string) + PDF_EXTENSION
  }

  def getTXTName(string: String): String = {
    FilenameUtils.getBaseName(string) + TXT_EXTENSION
  }

  def statusUpdate(jobId: String, fileName: String) = {
    val json = new JsonObject
    json.addProperty("jobId", jobId)
    json.addProperty("fileName", fileName)
    json.addProperty("jobType", "parse")
    json.addProperty("status", "")
    rabbit.publish(json, "status")
  }

  def statusUpdate(jobId: String, fileName: String, status: Boolean): Unit = {
    val json = new JsonObject
    json.addProperty("jobId", jobId)
    json.addProperty("fileName", fileName)
    json.addProperty("jobType", "parse")
    json.addProperty("status", status)
    rabbit.publish(json, "status")
  }

  def main() = {
    // Connect to Minio Client

    Logger.info("Initializing RabbitMQ")
    rabbit.listen("parse", consumer _, true)
  }

  main()
}

sealed trait ObjectType {
  def name: String
}

case object PDF extends ObjectType {
  val name = ".pdf"
}

case object TEXT extends ObjectType {
  val name = ".txt"
}

case class FilePermissionException(msg: String) extends RuntimeException(msg)

case class ExecutionFailedException(msg: String) extends RuntimeException(msg)